#/usr/bin/env bash

build() {
    pushd kaldi-builder
    $DOCKER_CMD build --rm -t kaldi-builder:$KALDI_BUILDER_VERSION .
    popd
}

source version.sh

mode="${1:-build}"

if [[ "${mode}" == "build" ]]; then
    build
fi
