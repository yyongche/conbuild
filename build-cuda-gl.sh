#!/usr/bin/env bash

CUDA_VERSION="11.6.2"
OS="ubuntu"
OS_VERSION="20.04"
CUDA_IMAGE="nvcr.io/nvidia/cuda:${CUDA_VERSION}-cudnn8-runtime-${OS}${OS_VERSION}"
LIBGLVND_VERSION="v1.2.0"

IMAGE_NAME="nvcr.io/nvidia/cuda-gl"
CGL_INTER_IMAGE_NAME_SUFFIX="/build-intermediate"
CGL_INTER_IMAGE_NAME="${IMAGE_NAME}${CGL_INTER_IMAGE_NAME_SUFFIX}"

build() {
    pushd cuda-gl
    podman build \
        -t "${CGL_INTER_IMAGE_NAME}:${CUDA_VERSION}-cudnn8-runtime-base-${OS}${OS_VERSION}" \
        --build-arg "from=${CUDA_IMAGE}" \
        opengl/base

    podman build \
        -t "${IMAGE_NAME}:${CUDA_VERSION}-cudnn8-runtime-${OS}${OS_VERSION}" \
        --build-arg "from=${CGL_INTER_IMAGE_NAME}:${CUDA_VERSION}-cudnn8-runtime-base-${OS}${OS_VERSION}" \
        --build-arg "LIBGLVND_VERSION=${LIBGLVND_VERSION}" \
        opengl/glvnd/runtime
    popd
}

save() {
    podman save "${IMAGE_NAME}:${CUDA_VERSION}-cudnn8-runtime-${OS}${OS_VERSION}" | \
        gzip > "cuda-gl_${CUDA_VERSION}.tar.gz"
}

mode="${1:-build}"

if [[ "${mode}" == "build" ]]; then
    build
elif [[ "${mode}" == "save" ]]; then
    save
fi
