#/usr/bin/env bash

build() {
    pushd nlp-runtime
    podman build --rm -t nlp-runtime:$NLP_RUNTIME_VERSION .
    popd
}

save() {
    podman save nlp-runtime:$NLP_RUNTIME_VERSION | \
        gzip > "nlp-runtime_${NLP_RUNTIME_VERSION}.tar.gz"
}

source version.sh

mode="${1:-build}"

if [[ "${mode}" == "build" ]]; then
    build
elif [[ "${mode}" == "save" ]]; then
    save
fi
