#!/usr/bin/env bash

build() {
    pushd eo-runtime-gpu
    podman build --rm -t eo-runtime-gpu:$EO_RUNTIME_GPU_VERSION .
    popd
}

save() {
    podman save eo-runtime-gpu:$EO_RUNTIME_GPU_VERSION | \
    	gzip > "eo-runtime-gpu_${EO_RUNTIME_GPU_VERSION}.tar.gz"
}

source version.sh

mode="${1:-build}"

if [[ "${mode}" == "build" ]]; then
    build
elif [[ "${mode}" == "save" ]]; then
    save
fi
