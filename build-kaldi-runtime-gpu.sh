#/usr/bin/env bash

build() {
    pushd kaldi-runtime-gpu
    $DOCKER_CMD build --build-arg BUILDER_VERSION=$KALDI_BUILDER_VERSION \
        --rm -t kaldi-runtime-gpu:$KALDI_RUNTIME_GPU_VERSION .
    popd
}

save() {
    $DOCKER_CMD save kaldi-runtime-gpu:$KALDI_RUNTIME_GPU_VERSION | \
        gzip > "kaldi-runtime-gpu_${KALDI_RUNTIME_GPU_VERSION}.tar.gz"
}

source version.sh

mode="${1:-build}"

if [[ "${mode}" == "build" ]]; then
    build
elif [[ "${mode}" == "save" ]]; then
    save
fi
