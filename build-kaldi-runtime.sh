#/usr/bin/env bash

build() {
    pushd kaldi-runtime
    $DOCKER_CMD build --build-arg BUILDER_VERSION=$KALDI_BUILDER_VERSION \
        --rm -t kaldi-runtime:$KALDI_RUNTIME_VERSION .
    popd
}

save() {
    $DOCKER_CMD save kaldi-runtime:$KALDI_RUNTIME_VERSION | \
        gzip > "kaldi-runtime_${KALDI_RUNTIME_VERSION}.tar.gz"
}

scan() {
    tmpdir="../tmp"
    [[ -d $tmpdir ]] || mkdir "${tmpdir}"
    TMPDIR=$tmpdir trivy image --image-src "${DOCKER_CMD}" \
        --scanners vuln -f table -o kaldi-runtime-trivy.log \
        kaldi-runtime:$KALDI_RUNTIME_VERSION
    rm -rf "${tmpdir}"
}

source version.sh

mode="${1:-build}"

if [[ "${mode}" == "build" ]]; then
    build
elif [[ "${mode}" == "save" ]]; then
    save
elif [[ "${mode}" == "scan" ]]; then
    scan
fi
