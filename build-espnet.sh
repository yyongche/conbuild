#!/usr/bin/env bash

BUILD_VERSION="gpu"
TORCH_VERSION="2.0.1"
ESPNET_VERSION="v.202402"

build() {
    pushd espnet
    bash build.sh --build-ver $BUILD_VERSION \
        --th-ver $TORCH_VERSION --espnet-ver $ESPNET_VERSION \
        build
    popd
}

save() {
    podman save espnet/espnet:$ESPNET_VERSION | \
        gzip > "espnet_${ESPNET_VERSION}.tar.gz"
}

mode="${1:-build}"

if [[ "${mode}" == "build" ]]; then
    build
elif [[ "${mode}" == "save" ]]; then
    save
fi
