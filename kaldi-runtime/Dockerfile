ARG BUILDER_VERSION="1.2"

FROM kaldi-builder:${BUILDER_VERSION} AS builder
ADD kaldi_v3.tar.gz /app/
COPY check_dependencies.sh install_liblbfgs.sh /app/kaldi_v3/tools/extras/
COPY liblbfgs-1.10.tar.gz openfst-1.6.7.tar.gz sctk-2.4.10-20151007-1312Z.tar.bz2 sph2pipe_v2.5.tar.gz /app/kaldi_v3/tools/
COPY srilm-1.7.1.tar.gz /app/kaldi_v3/tools/srilm.tgz
WORKDIR /app/kaldi_v3
RUN cd tools && make && ./install_srilm.sh
RUN cd src && ./configure --use-cuda=no && make

RUN mkdir /app/kaldi && cp src/.version /app/kaldi/ && \
    cd src && find . -executable -type f -print | xargs -I {} cp --parents {} /app/kaldi && \
    rm /app/kaldi/configure && rm -r /app/kaldi/doc /app/kaldi/probe
RUN mkdir /app/kaldi/lib /app/kaldi/openfstbin /app/kaldi/sctkbin /app/kaldi/srilmbin && \
    find tools/ -executable -type f -name 'sph2pipe' -print | xargs -I {} cp {} /app/kaldi/base && \
    find tools/ -name '*.so*' -not -path '*/.libs/*' -print | xargs -I {} cp -P {} /app/kaldi/lib && \
    find tools/openfst/bin -executable -type f -print | xargs -I {} cp {} /app/kaldi/openfstbin && \
    find tools/sctk/bin -executable -type f -print | xargs -I {} cp {} /app/kaldi/sctkbin && \
    cd tools/srilm/bin && find . -executable -type f -not -name '*.~*' -print | xargs -I {} cp --parents {} /app/kaldi/srilmbin

FROM ubuntu:24.04 AS runtime
ENV DEBIAN_FRONTEND noninteractive
COPY --chmod=755 add_DSOcerts.sh /root/add_DSOcerts.sh
RUN /root/add_DSOcerts.sh && apt-get update && \
    apt-get install -y zlib1g libatlas3-base && \
    apt-get install -y python3 python3-pip python3-defusedxml python3-numpy python3-scipy && \
    apt-get install -y python3-future python3-typing-extensions && \
    apt-get install -y icu-devtools sox libsox-fmt-all ffmpeg && \
    apt-get clean && rm -rf /var/lib/apt/lists/* && \
    pip install --no-cache-dir --break-system-packages sox ffmpeg-python

FROM runtime
COPY --from=builder /app/kaldi /opt/kaldi
ENV KALDI_ROOT /opt/kaldi
