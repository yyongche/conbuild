#!/usr/bin/env bash

build() {
    pushd sonar-pylint
    podman build --rm -t sonar-pylint:$SONAR_PYLINT_VERSION .
    popd
}

save() {
    podman save sonar-pylint:$SONAR_PYLINT_VERSION | \
    	gzip > "sonar-pylint_${SONAR_PYLINT_VERSION}.tar.gz"
}

source version.sh

mode="${1:-build}"

if [[ "${mode}" == "build" ]]; then
    build
elif [[ "${mode}" == "save" ]]; then
    save
fi
